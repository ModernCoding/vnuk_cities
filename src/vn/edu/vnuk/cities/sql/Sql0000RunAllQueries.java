package vn.edu.vnuk.cities.sql;

import java.sql.Connection;
import java.sql.SQLException;

import vn.edu.vnuk.cities.ConnectionFactory;

public class Sql0000RunAllQueries {

	public static void main(String[] args) throws SQLException {
		
	/*
		=============================================================================
		
			DATABASE
			
		=============================================================================
	*/
	
		Connection connectionDb = new ConnectionFactory()
				.getConnection("jdbc:mysql://localhost/");
		
		
		//	Sql1000 series: creating new DB from scratch
		
		new Sql1000DropDatabase(connectionDb).run();
		new Sql1010CreateDatabase(connectionDb).run();

		connectionDb.close();
		
		
	/*
		=============================================================================
		
			TABLES
			
		=============================================================================
	*/
		
		Connection connectionTable = new ConnectionFactory()
				.getConnection("jdbc:mysql://localhost/vnuk_cities");
		
		
		//	Sql4000 series: creating new tables from scratch
		
		new Sql4010CreateContinents(connectionTable).run();
		new Sql4020CreateCountries(connectionTable).run();
		new Sql4030CreateCities(connectionTable).run();

		
		//	Sql7000 series: inserting data into tables
		
		new Sql7010InsertIntoContinents(connectionTable).run();
		new Sql7020InsertIntoCountries(connectionTable).run();
		new Sql7030InsertIntoCities(connectionTable).run();
		
		
		connectionTable.close();
	}

}
