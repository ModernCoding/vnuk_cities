package vn.edu.vnuk.cities.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Sql7010InsertIntoContinents {
	private final Connection connection;
	private final String sqlQuery;
	
	public Sql7010InsertIntoContinents(Connection connection) {
		this.connection = connection;
		
		this.sqlQuery = "INSERT INTO continents (label) "
				+ 	"values "
				+ 	"('Africa'), "
				+ 	"('North America'), "
				+ 	"('South America'), "
				+ 	"('Antarctica'), "
				+ 	"('Asia'), "
				+ 	"('Europe'), "
				+ 	"('Oceania')"
				+ ";"
			;
	}
	
	public void run() throws SQLException {

		System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println(">  Sql7010InsertIntoContinents started");
		
		try {
			PreparedStatement statement = connection.prepareStatement(sqlQuery);
			statement.execute();
			statement.close();
	        System.out.println("   DATA successfully loaded in \'continents\'");
		
		}
		
		catch (Exception e) {
	        e.printStackTrace();
	        connection.close();
		}
		
		finally {
			System.out.println("<  Sql7010InsertIntoContinents ended");
			System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			System.out.println("");
		}
			
	}

}
